#!/bin/sh
set -ex

yum install -y python3-pip
pip3 install -r requirements-dev.txt
