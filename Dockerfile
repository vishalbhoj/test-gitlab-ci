FROM public.ecr.aws/lambda/python:3.8

RUN yum install -y tar wget
# Extract kernel-baseline tarballs
RUN wget -q https://builds.tuxbuild.com/kernel-baseline/common.git.tar && \
    tar -xf common.git.tar -C / && \
    rm -rf common.git.tar && df -h && \
    wget -q https://builds.tuxbuild.com/kernel-baseline/linux-baseline.git.tar && \
    tar -xf linux-baseline.git.tar -C / &&  \
    rm *.tar

RUN yum install -y git tar wget
