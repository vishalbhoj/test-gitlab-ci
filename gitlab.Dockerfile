FROM python:3.8-buster

RUN apt-get update && \
    apt-get install -y zstd jq make curl strace && \
    rm -rf /var/lib/apt/lists/*

RUN pip install awscli pipenv lambpy tuxbuild tuxsuite && \
    aws configure set default.region us-east-1

RUN mkdir -p ~/.config/tuxsuite/

COPY gitlab-config.ini /root/.config/tuxsuite/config.ini

run cat /root/.config/tuxsuite/config.ini
